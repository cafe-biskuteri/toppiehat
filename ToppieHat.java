
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;
import java.util.*;

public class
ToppieHat extends JPanel
implements Runnable, KeyListener, ActionListener {

	private double
	hatX, hatY, hatYVel, hatYAccel,
	yVelIncr, minYVel, maxYVel;

	private double[]
	pillarXs, pillarYs;

	private State
	state;

	private double
	bgX;

	private int
	bgW;

	private Image
	hat,
	bg,
	downPillar,
	upPillar,
	unpressed,
	pressed;

	private JButton
	shareButton;

	private Thread
	gameThread;

	private String
	instanceUrl,
	accessToken;

//	 -	-%-	 -

	private static final int
	GAMERATE = 30;

	private static final double
	MAXVEL = 40;

//	---%-@-%---

	public void
	run()
	{
		Toolkit TK = Toolkit.getDefaultToolkit();
		Thread t = Thread.currentThread();
		while (!t.isInterrupted()) try
		{
			advanceBg();
			if (state == State.PLAYING) advanceHat();
			repaint();
			TK.sync();
			Thread.sleep(1000 / GAMERATE);
		}
		catch (InterruptedException eIt)
		{
			t.interrupt();
		}
	}

	private void
	advanceHat()
	{
		hatX += 2.0;

		double dYVel = hatYAccel / GAMERATE;
		hatYVel = limit(hatYVel + dYVel, minYVel, maxYVel);
		double dY = hatYVel / GAMERATE;
		hatY = hatY + dY;

		int minY = 0, maxY = getHeight() + 24;
		if (hatY != limit(hatY, minY, maxY))
			state = State.GAMEOVER;

		int pw = downPillar.getWidth(this);
		int ph = downPillar.getHeight(this);
		int hw = hat.getWidth(this);
		int hh = hat.getHeight(this);
		boolean hit = false;
		boolean crossed2 = false;
		for (int o = 0; o < pillarXs.length; ++o)
		{
			double x = pillarXs[o];
			boolean left = (hatX + hw/2) >= (x - pw/2);
			boolean right = (hatX - hw/2) > (x + pw/2);
			crossed2 |= o == 1 && right;

			double y = pillarYs[o];
			boolean top = (hatY + hh/2) >= y + 50;
			boolean bottom = (hatY - hh/2) <= y - 50;

			boolean crossing = left && !right;
			boolean ok = !crossing || (!top && !bottom);
			hit |= !ok;
		}

		if (crossed2)
		{
			pillarXs[0] = pillarXs[1];
			pillarXs[1] = pillarXs[2];
			pillarXs[2] = pillarXs[3];
			pillarXs[3] = pillarXs[4];
			double add = 150 + (25 * Math.random());
			pillarXs[4] = pillarXs[3] + add;

			pillarYs[0] = pillarYs[1];
			pillarYs[1] = pillarYs[2];
			pillarYs[2] = pillarYs[3];
			pillarYs[3] = pillarYs[4];
			pillarYs[4] = genAppropriateY(pillarYs[3]);
		}

		if (hit) state = State.GAMEOVER;
		// We ought to have an animation.
	}

	private void
	advanceBg()
	{
		bgX += 1.5;
		if (bgX >= bgW) bgX = 0;
	}

	protected void
	paintComponent(Graphics g)
	{
		((Graphics2D)g).setRenderingHint(
			RenderingHints.KEY_TEXT_ANTIALIASING,
			RenderingHints.VALUE_TEXT_ANTIALIAS_ON
		);

		shareButton.setVisible(state == State.GAMEOVER);

		paintBg(g);
		if (state == State.WAITING) paintWaiting(g);
		if (state == State.PLAYING) paintPlaying(g);
		if (state == State.GAMEOVER) paintGameOver(g);
	}

	protected void
	paintBg(Graphics g)
	{
		int w = getWidth();
		g.drawImage(bg, (int)(0 - bgX), 0, this);
		g.drawImage(bg, (int)(bgW - bgX), 0, this);
	}

	protected void
	paintPlaying(Graphics g)
	{
		int w = getWidth(), h = getHeight();

		final double TAU = 2 * Math.PI;
		double pitch =
			(hatYVel - minYVel) / (maxYVel - minYVel);
		double angle = 
			(TAU * -1/8) + pitch * (TAU * 1/4);
		int hw = hat.getWidth(this);
		int hh = hat.getHeight(this);
		int hx = w * 1/6;
		int hy = (int)(h - hatY);
		Graphics2D g2d = (Graphics2D)g;

		g.translate(hx, hy);
		g2d.rotate(-angle);
		g.drawImage(hat, -hw/2, -hh/2, this);
		g2d.rotate(angle);
		g.translate(-hx, -hy);

		int pw = downPillar.getWidth(this);
		int ph = downPillar.getHeight(this);
		for (int o = 0; o < pillarXs.length; ++o)
		{
			double dx = pillarXs[o] - hatX;
			int y = (int)(h - pillarYs[o]);
			int x = (int)(w * 1/6 + dx);

			int l = x - (pw / 2);
			int yu = y - 50 - ph;
			int yd = y + 50;

			g.drawImage(downPillar, l, yu, this);
			g.drawImage(upPillar, l, yd, this);
		}

		g.setColor(Color.WHITE);
		int score = (int)hatX;
		g.drawString(Integer.toString(score), 8, 24);
	}

	protected void
	paintWaiting(Graphics g)
	{
		int w = getWidth(), h = getHeight();

		Color c1 = new Color(128*9/6, 112*9/6, 96*9/6);
		Color c2 = new Color(0, 0, 0, 50);

		Font f1 = new Font("Linux Libertine O", Font.ITALIC, 44);
		FontMetrics fm1 = g.getFontMetrics(f1);
		String s1 = "Toppie Hat";
		int x1 = w/2 - fm1.stringWidth(s1)/2;
		int y1 = h * 1/3;

		Font f2 = new Font("Linux Libertine O", Font.PLAIN, 18);
		FontMetrics fm2 = g.getFontMetrics(f2);
		String s2 = "Hit enter to begin..";
		int x2 = w/2 - fm2.stringWidth(s2)/2;
		int y2 = h * 1/2;

		g.setFont(f1);
		g.translate(4, 4);
		g.setColor(c2);
		g.drawString(s1, x1, y1);
		g.translate(-8, -8);
		g.setColor(c1);
		g.drawString(s1, x1, y1);
		g.translate(4, 4);

		g.setFont(f2);
		g.setColor(c1);
		g.drawString(s2, x2, y2);
	}

	protected void
	paintGameOver(Graphics g)
	{
		int w = getWidth(), h = getHeight();

		Color c1 = new Color(128*5/3, 128*5/3, 96*5/3);

		int score = (int)hatX;
		String sscore = Integer.toString(score);
		String reaction = score > 500 ? "!" : "..";
		
		Font f1 = new Font("Linux Libertine O", Font.ITALIC, 24);
		FontMetrics fm1 = g.getFontMetrics(f1);
		String s1 = "You got " + sscore + " points" + reaction;
		int x1 = w/2 - fm1.stringWidth(s1)/2;
		int y1 = h * 5/16;

		Font f2 = new Font("Linux Libertine O", Font.PLAIN, 18);
		FontMetrics fm2 = g.getFontMetrics(f2);
		String s2 = "Hit enter to return..";
		int x2 = w/2 - fm2.stringWidth(s2)/2;
		int y2 = h * 7/16;

		String s3 = "Or press the Mastodon logo";
		String s4 = "to share your results!";
		int x3 = w/2 - fm2.stringWidth(s3)/2;
		int y3 = y2 + fm2.getHeight() * 2;
		int x4 = w/2 - fm2.stringWidth(s4)/2;
		int y4 = y3 + fm2.getHeight();

		g.setFont(f1);
		g.setColor(c1);
		g.drawString(s1, x1, y1);

		g.setFont(f2);
		g.setColor(c1);
		g.drawString(s2, x2, y2);
		g.drawString(s3, x3, y3);
		g.drawString(s4, x4, y4);

		shareButton.setSize(shareButton.getPreferredSize());
		int x5 = w/2 - shareButton.getWidth()/2;
		int y5 = y4 + 12;
		shareButton.setLocation(x5, y5);
	}

	public void
	keyPressed(KeyEvent eK)
	{
		int keyCode = eK.getKeyCode();

		if (state == State.PLAYING) switch (keyCode)
		{
			case KeyEvent.VK_SPACE:
			case KeyEvent.VK_W:
			case KeyEvent.VK_K:
				hatYVel = limit(hatYVel + yVelIncr, minYVel, maxYVel);
				break;
		}
		else if (state == State.WAITING) switch (keyCode)
		{
			case KeyEvent.VK_ENTER:
			case KeyEvent.VK_SPACE:
				initGame();
				state = State.PLAYING;
				break;
		}
		else if (state == State.GAMEOVER) switch (keyCode)
		{
			case KeyEvent.VK_ESCAPE:
			case KeyEvent.VK_ENTER:
				state = State.WAITING;
				break;
		}
	}

	private void
	initGame()
	{	
		hatX = 0;
		hatY = 300;
		hatYVel = 0;

		pillarXs = new double[5];
		pillarXs[0] = 150 + (25 * Math.random());
		pillarXs[1] = 300 + (25 * Math.random());
		pillarXs[2] = 450 + (25 * Math.random());
		pillarXs[3] = 600 + (25 * Math.random());
		pillarXs[4] = 750 + (25 * Math.random());
		pillarYs = new double[5];
		pillarYs[0] = 100 + (300 * Math.random());
		pillarYs[1] = genAppropriateY(pillarYs[0]);
		pillarYs[2] = genAppropriateY(pillarYs[1]);
		pillarYs[3] = genAppropriateY(pillarYs[2]);
		pillarYs[4] = genAppropriateY(pillarYs[3]);
	}

	public void
	actionPerformed(ActionEvent eA)
	{
		assert eA.getSource() == shareButton;

		if (instanceUrl == null)
		{
			JOptionPane.showMessageDialog(
				this,
				"We don't have the access token to\n"
				+ "your Mastodon account.. You have\n"
				+ "to put your details in a text file.\n"
				+ "Ask the developer for details.."
			);
		}
		else try
		{
			sendResults();
			state = State.WAITING;
		}
		catch (IOException eIo)
		{
			JOptionPane.showMessageDialog(
				this,
				"We couldn't upload your results :(\n"
				+ eIo.getMessage()
			);
		}
	}

	private double
	genAppropriateY(double leftY)
	{
		int h = getHeight();

		double topY = leftY + 100;
		double bottomY = leftY - 100;
		double offset = 0;

		if (topY > 400) offset = -50 + (400 - topY);
		if (bottomY < 50) offset = 50 + (50 - bottomY);
		topY += offset;
		bottomY += offset;

		return bottomY + (Math.random() * (topY - bottomY));
	}

	public void
	keyReleased(KeyEvent eK) { }

	public void
	keyTyped(KeyEvent eK) { }

	private void
	sendResults()
	throws IOException
	{
		assert state == State.GAMEOVER;
		assert instanceUrl != null;
		assert accessToken != null;

		int joystick1 = 0x1F579;
		int joystick2 = 0xFE0F;
		StringBuilder b = new StringBuilder();
		b.append(Character.highSurrogate(joystick1));
		b.append(Character.lowSurrogate(joystick1));
		b.append((char)joystick2);
		b.append(" I got ");
		b.append(Integer.toString((int)hatX));
		b.append(" points in Toppie Hat!");
		String text = encoded(b.toString());

		URL endp = new URL(instanceUrl + "/api/v1/statuses");
		HttpURLConnection conn;
		conn = (HttpURLConnection)endp.openConnection();
		String s1 = "Bearer " + accessToken;
		conn.setRequestProperty("Authorization", s1);
		String s2 = Integer.toString(text.hashCode());
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.connect();

		OutputStream os = conn.getOutputStream();
		Writer output = new OutputStreamWriter(os);
		output.write("status=" + text);
		output.write("&visibility=unlisted");
		output.close();

		int code = conn.getResponseCode();
		if (code >= 300)
			throw new IOException("HTTP error: " + code);
	}

//	 -	-%-	 -

	private static double
	limit(double v, double min, double max)
	{
		return v < min ? min : v > max ? max : v;
	}

//	---%-@-%---

	private class
	Frame extends JFrame {

		Frame()
		{
			super("Toppie Hat");
			add(ToppieHat.this);
			pack();
			setResizable(false);
			setDefaultCloseOperation(EXIT_ON_CLOSE);
			//setLocationByPlatform(true);
			setLocationRelativeTo(null);
		}
		
	}

	private static enum
	State {

		WAITING,
		PLAYING,
		GAMEOVER

	}

//	---%-@-%---

	public static void
	main(String... args)
	{
		new ToppieHat().new Frame().setVisible(true);
	}

//	---%-@-%---

	public
	ToppieHat()
	{
		hat = img("TopHat.png");
		bg = img("Background.png");
		downPillar = img("DownPillar.png");
		upPillar = img("UpPillar.png");
		unpressed = img("MastodonUnpressed.png");
		pressed = img("MastodonPressed.png");

		shareButton = new JButton();
		shareButton.setBorder(null);
		shareButton.setContentAreaFilled(false);
		shareButton.setMargin(new Insets(0, 0, 0, 0));
		shareButton.setIcon(new ImageIcon(unpressed));
		shareButton.setPressedIcon(new ImageIcon(pressed));
		shareButton.setPreferredSize(new Dimension(
			unpressed.getWidth(this),
			unpressed.getHeight(this)
		));
		shareButton.addActionListener(this);
		this.setLayout(null);
		this.add(shareButton);

		minYVel = -200;
		maxYVel = 160;
		yVelIncr = 160;
		hatYAccel = -200;
		state = State.WAITING;

		bgX = 0;
		bgW = bg.getWidth(this);

		setPreferredSize(new Dimension(300, 450));
		setSize(getPreferredSize());
		setFocusable(true);
		this.addKeyListener(this);

		try
		{
			loadMastodonData();
		}
		catch (IOException eIo)
		{
			instanceUrl = null;
			accessToken = null;
		}

		(gameThread = new Thread(this)).start();
	}

//	 -	-%-	 -

	private void
	loadMastodonData()
	throws IOException
	{
		String home = System.getProperty("user.home");
		String path = "/.config/toppiehat.dsv";
		File file = new File(home + path);

		FileReader r = new FileReader(file);
		BufferedReader br = new BufferedReader(r);
		String line1 = br.readLine();
		String line2 = br.readLine();
		if (line1 == null || line2 == null) {
			throw new IOException("File is missing lines..");
		}

		instanceUrl = line1;
		accessToken = line2;
	}

//	 -	-%-	 -

	private static Image
	img(String path)
	{
		URL url = ToppieHat.class.getResource(path);
		if (url == null) return null;
		return new ImageIcon(url).getImage();
	}

	private static String
    encoded(String string)
    {
        try {
            if (string == null) return null;
            return URLEncoder.encode(string, "UTF-8");
        }
        catch (UnsupportedEncodingException eUe) {
            assert false;
            return null;
        }
    }

} 
